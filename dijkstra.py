import PySimpleGUI as sg

class Grafo:
    def distanciaMin(self, distancia, lista):
        minimo = float("Inf") # valor infitino
        indiceMin = 0
        for i in range(len(distancia)):
            if distancia[i] < minimo and i in lista: # se o valor contido no end i for menor que infinito
                minimo = distancia[i]
                indiceMin = i
        return indiceMin

    def dijkstra(self, grafo, origem, destino):
        linha = len(grafo) #qtd de linhas no grafo
        coluna = len(grafo[origem]) #qtd de colunas no grafo
        distancia = [float("Inf")] * linha # n de linhas * a lista[inf]
        distancia[origem] = 0 # nao percorreu distancia alguma
        lista = []
        for i in range(linha):
            lista.append(i) # adicionando valores de 0 a 8 a lista
        while lista:
            v = self.distanciaMin(distancia, lista) # chamando a funcao
            lista.remove(v) # remove o indiceMin
            for i in range(coluna): # percorrendo as colunas
                if grafo[v][i] and i in lista:
                    if distancia[v] + grafo[v][i] < distancia[i]:
                        distancia[i] = distancia[v] + grafo[v][i] #somando os pesos das arestas
        print("Origem  Destino  Distancia")
        print("     %d     |    %d    |    %d " % (origem, destino, distancia[destino]))

g = Grafo()

#matriz de adjacencia
grafo = [[0, 4, 0, 7, 0, 0, 0, 0, 0],#0
         [4, 0, 8, 0,10, 0, 0, 0, 0],#1
         [0, 8, 0, 0, 0, 2, 0, 0, 0],#2
         [7, 0, 0, 0, 4, 0,13, 0, 0],#3
         [0,10, 0, 4, 0, 6, 0,11, 0],#4
         [0, 0, 2, 0, 6, 0, 0, 0, 1],#5
         [0, 0, 0,13, 0, 0, 0, 0, 0],#6
         [0, 0, 0, 0,11, 0, 0, 0, 5],#7
         [0, 0, 0, 0, 0, 1, 0, 5, 0]]#8

sg.theme('DarkGrey')
layout = [
    [sg.Text('Origem da viagem: '),sg.Input(key = 'origem', size=(27,1))],
    [sg.Text('Destino:                '),sg.Input(key = 'destino', size=(27,1))],
    [sg.Button(' Ok ')],
    [sg.Output(size=(43,5))],
    [sg.Image('grafo.png',size=(320,180))]]
janela = sg.Window('Shortest Path', layout)

while True:
    eventos, valores = janela.read()
    a = int(valores['origem'])
    b = int(valores['destino'])
    if 0 <= a <= 8 and 0 <= b <= 8:
        g.dijkstra(grafo, a, b)
    else:
        print('Vertice nao encontrado.')